EXCEL MERGER
============

Table of contents
-----------------
1. Installation on Windows
2. Installation on OSX / Linux
3. Usage on Windows
4. Usage on OSX / Linux

1 - Installation on Windows
--------------------------
1. If PHP is not installed on your system you need to install it first. The easiest way to do this is by using the XAMPP installer.
	1. Download the installer at https://www.apachefriends.org/index.html
	2. Install only the required options (PHP and Apache) and install in the default directory\*
	3. When Windows Firewall asks if you want to permit Apache to bypass the firewall you can select cancel, because we don't really need Apache
	<br />\* If you do not install in the default directory you need to edit excel_merge.bat in a text editor and change the location of the PHP executable accordingly
2. Download the ZIP file from https://bitbucket.org/eawenden/excel-merger/downloads
3. Extract the ZIP file into the desired folder

2 - Installation on OSX / Linux
------------------------------
1. Download the ZIP file from https://bitbucket.org/eawenden/excel-merger/downloads
2. Extract the ZIP file into the desired folder

3 - Usage on Windows
--------------------
1. Place the input files in the input folder
2. Double click excel_merge.bat
3. Check output for errors & warnings before closing the command prompt window
4. Find your merged Excel file in the output folder

4 - Usage on OSX / Linux
------------------------
1. Place the input files in the input folder
2. Double click excel_merge.sh
3. Check output for errors & warnings before closing the terminal window
4. Find your merged Excel file in the output folder