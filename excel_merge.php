<?php

// Include PHPExcel
require_once dirname(__FILE__).DIRECTORY_SEPARATOR.'Classes'.DIRECTORY_SEPARATOR.'PHPExcel.php';

$excelMerger = new ExcelMerger(dirname(__FILE__).DIRECTORY_SEPARATOR.'input', dirname(__FILE__).DIRECTORY_SEPARATOR.'output');
$excelMerger->run();

/**
 * Class ExcelMerger
 */
class ExcelMerger {
    protected $columnTemplate;
    protected $columnCurrent;

    protected $inputDirectory;
    protected $outputDirectory;
    protected $outputFilename;

    protected $output;
    protected $log;

    protected $startTime;

    /**
     * @param $inputDirectory
     * @param $outputDirectory
     */
    public function __construct($inputDirectory, $outputDirectory)
    {
        // Set memory limit & execution time
        ini_set('memory_limit', '2G');
        set_time_limit(0);

        // Set start time
        $this->startTime = new DateTime();

        // Set the input directory
        $this->inputDirectory = $inputDirectory;

        // Create a new directory in the output directory
        $this->outputDirectory = $outputDirectory.DIRECTORY_SEPARATOR.date('Y-m-d_H.i.s');
        mkdir($this->outputDirectory);

        // Set the column template to null
        $this->columnTemplate = null;

        // Create a PHPExcel object for the output
        $this->output = new PHPExcel();
        $this->output->getProperties()->setCompany('NOA');

        // Create a log file
        $this->log = fopen($this->outputDirectory.DIRECTORY_SEPARATOR.'log.txt', 'a+');
    }


    /**
     * Output a message to the terminal and log file
     *
     * @param $message
     */
    protected  function message($message)
    {
        echo "$message\n";
        fwrite($this->log, "$message\n");
    }


    /**
     * Prints a header to the log and terminal
     */
    protected function printHeader()
    {
        $this->message('################################');
        $this->message('## EXCEL MERGER               ##');
        $this->message('##   Written by Sophie & Erik ##');
        $this->message('################################');
        $this->message('');
        $this->message('Started merging at '.$this->startTime->format('H:i:s \o\n d-m-Y'));
        $this->message('');
    }


    /**
     * Runs the Excel Merge
     */
    public function run()
    {
        try {
            $this->printHeader();

            // Iterate over the files in the input directory
            chdir($this->inputDirectory);
            foreach (scandir('.') as $file) {
                if ($file[0] == '~') {
                    continue;
                } elseif (pathinfo($file, PATHINFO_EXTENSION) == 'xlsx') {
                    // Generate output file name
                    if ($this->outputFilename == null || $this->outputFilename == '.xls') {
                        $this->outputFilename = preg_replace('/(?:\d)*[- _](?:\d)+(\.xlsx)/', '.xls', $file);
                    }

                    // Echo the name of the current sheet
                    $this->message("Processing file {$file}");

                    // Create a reader for the file
                    /* @var PHPExcel_Reader_Abstract $excelReader */
                    $excelReader = PHPExcel_IOFactory::createReaderForFile($file);
                    $excelReader->setReadDataOnly(true);    

                    // Open the file
                    /* @var PHPExcel $excelObject */
                    $excelObject = $excelReader->load($file);
                    $excelObject->setActiveSheetIndex();
                    $excelWorksheet = $excelObject->getActiveSheet();
    
                    // Process the worksheet
                    $this->processWorksheet($excelWorksheet);
                }
            }

            // Write the output files
            if ($this->columnTemplate != null) {
                $this->writeOutputFiles();
            } else {
                $this->message('No .xlsx files found in input directory!');
                fclose($this->log);
            }
        } catch (Exception $e) {
            $this->message('Something went wrong!');
            $this->message($e->getMessage());
        }

        // Ask for confirmation before closing
        if(!defined("STDIN")) {
            define("STDIN", fopen('php://stdin','r'));
        }
        echo "\nPress enter to exit!\n";
        fread(STDIN, 1);
    }


    /**
     * Processes a worksheet
     *
     * @param PHPExcel_Worksheet $worksheet
     */
    protected function processWorksheet(PHPExcel_Worksheet $worksheet)
    {
        // If this is the first worksheet we process we need to set the columnTemplate
        if ( ! $this->columnTemplate) {
            $this->processFirstWorksheet($worksheet);
        } else { // Otherwise we process the worksheet using the template
            $this->processSubsequentWorksheet($worksheet);
        }

        // Next we process the rows
        $this->processRows($worksheet);
    }


    /**
     * Processes the column header of the first worksheet
     *
     * @param PHPExcel_Worksheet $worksheet
     */
    protected function processFirstWorksheet(PHPExcel_Worksheet $worksheet)
    {
        // Reset column template
        $this->columnTemplate = array();

        // Process the first row for column names
        $row = $worksheet->getRowIterator(1)->current();

        foreach ($row->getCellIterator() as $cell) {
            /* @var PHPExcel_Cell $cell */
            if ($cell->getValue() != '') {
                // See if the column already exists in the template and exit if it does.
                if (array_key_exists((string)$cell->getValue(), $this->columnTemplate)) {
                    $this->message('!! Excel file contains duplicate column: '.$cell->getValue().'!');
                }

                // Add the column header to the template
                $this->columnTemplate[(string)$cell->getValue()] = $cell->getColumn();
            }
        }

        // Set the template as the current columns
        $this->columnCurrent = $this->columnTemplate;

        // Prepare the output file
        $this->prepareOutputFile();
    }


    /**
     * Process the header of any subsequent worksheet
     *
     * @param PHPExcel_Worksheet $worksheet
     */
    protected function processSubsequentWorksheet(PHPExcel_Worksheet $worksheet)
    {
        // Reset column template
        $this->columnCurrent = array();

        // Process the first row for column names
        $row = $worksheet->getRowIterator(1)->current();

        foreach ($row->getCellIterator() as $cell) {
            /* @var PHPExcel_Cell $cell */
            if ($cell->getValue() != '') {
                // See if the column already exists in the current column index and exit if it does.
                if (array_key_exists((string)$cell->getValue(), $this->columnCurrent)) {
                    $this->message('!! Excel file contains duplicate column: '.$cell->getValue().'!');
                }

                // Add the column header to the column index
                $this->columnCurrent[(string)$cell->getValue()] = $cell->getColumn();
            }
        }

        // Check if there are any new columns in the current worksheet and add them to the template
        foreach ($this->columnCurrent as $label => $column) {
            if ( ! array_key_exists($label, $this->columnTemplate)) {
                $this->columnTemplate[$label] = PHPExcel_Cell::stringFromColumnIndex(PHPExcel_Cell::columnIndexFromString($this->output->getActiveSheet()->getHighestColumn(1)));
                $this->output->getActiveSheet()->setCellValue($this->columnTemplate[$label].'1', $label);
                $this->message("++ column added: {$label}");
            }
        }

        // Check if there are any columns missing in the current worksheet
        foreach ($this->columnTemplate as $label => $column) {
            if ( ! array_key_exists($label, $this->columnCurrent)) {
                $this->message("-- column missing: {$label}");
            }
        }
    }


    /**
     * Prepares the columns in the output file
     */
    protected function prepareOutputFile()
    {
        // Iterate over all columns
        foreach ($this->columnTemplate as $label => $column) {
            // Write the column header
            $cell = $this->output->getActiveSheet()->getCell("{$column}1");
            $cell->setValue($label);
        }
    }


    /**
     * Processes the data from a worksheet
     *
     * @param PHPExcel_Worksheet $worksheet
     */
    protected function processRows(PHPExcel_Worksheet $worksheet)
    {
        // Get the output sheet
        $outputSheet = $this->output->getActiveSheet();

        // Add the worksheet to the output file so we can copy styling (doesn't work yet)
        $worksheet->setTitle('TEMP');
        $this->output->addExternalSheet($worksheet);
        $this->output->setActiveSheetIndex(1);
        $worksheet = $this->output->getActiveSheet();

        // Iterate over all rows
        foreach ($worksheet->getRowIterator(2) as $row) {
            /* @var PHPExcel_Worksheet_Row $row */
            // Find the target column in the output file
            $targetRow = $this->output->getSheet(0)->getHighestRow()+1;

            // For all columns in the template
            foreach ($this->columnCurrent as $label => $column) {
                // Set coordinates
                $targetCoordinate = "{$this->columnTemplate[$label]}{$targetRow}";
                $sourceCoordinate = $column.$row->getRowIndex();

                // Find the input cell
                $cell = $worksheet->getCell($sourceCoordinate);

                if ($cell->getFormattedValue() != '') {
                    // Write the value to the output file
                    $outputSheet->setCellValue($targetCoordinate, $cell->getFormattedValue());

                    // TODO Copy the styling to the output file
                    //$outputSheet->duplicateStyle($worksheet->getStyle($sourceCoordinate), $targetCoordinate);
                }
            }
        }

        // Remove the worksheet from the output file
        $this->output->removeSheetByIndex(1);
        $this->output->setActiveSheetIndex(0);
    }


    /**
     * Writes the output files to disk
     */
    protected function writeOutputFiles()
    {
        // Set the width for all columns to 20
        $this->output->getSheet(0)->getDefaultColumnDimension()->setWidth(18);

        // Set the row height to auto
        $this->output->getSheet(0)->getRowDimension('1')->setRowHeight(-1);

        // Make the first row bold
        $this->output->getActiveSheet()->getStyle(
            "A1:".$this->output->getActiveSheet()->getHighestColumn()."1"
        )->getFont()->setBold(true)->setName('Verdana')->setSize(8);

        // And activate text wrap on the first row
        $this->output->getActiveSheet()->getStyle(
            "A1:".$this->output->getActiveSheet()->getHighestColumn()."1"
        )->getAlignment()->setWrapText(true);

        // Set font for the rest of the document
        $this->output->getActiveSheet()->getStyle(
            "A2:".$this->output->getActiveSheet()->getHighestColumn().$this->output->getActiveSheet()->getHighestRow()
        )->getFont()->setName('Verdana')->setSize(8);

        // Freeze the first row
        $this->output->getActiveSheet()->freezePane('A2');

        // Check to see if we have a valid output filename
        if ($this->outputFilename == '.xls') {
            $this->outputFilename = 'output.xls';
        }

        // Write the output file
        $excelWriter = PHPExcel_IOFactory::createWriter($this->output, 'Excel5');
        $excelWriter->save($this->outputDirectory.DIRECTORY_SEPARATOR.$this->outputFilename);

        // Move input files to output directory
        $sourceOutputDirectory = $this->outputDirectory.DIRECTORY_SEPARATOR.'source';
        mkdir($sourceOutputDirectory);
        foreach (scandir('.') as $file) {
            if (pathinfo($file, PATHINFO_EXTENSION) == 'xlsx') {
                // Try to move Excel files to different folder
                try{
                    rename($file, $sourceOutputDirectory.DIRECTORY_SEPARATOR.$file);
                } catch (Exception $e) {
                    $this->message($e->getMessage());
                }
            }
        }

        // Write footer
        $finishTime = new DateTime();
        $this->message('');
        $this->message('Finished merging at '.$finishTime->format('H:i:s \o\n d-m-Y'));
        $this->message('Merging took '.date_diff($finishTime, $this->startTime)->format('%H:%I:%S'));

        // Close the log file
        fclose($this->log);
    }
}
